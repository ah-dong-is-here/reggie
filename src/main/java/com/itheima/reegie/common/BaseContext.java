package com.itheima.reegie.common;

import org.springframework.context.annotation.Configuration;

public class BaseContext {
//TODO：到时候改成过滤器就行
    //已改

    // 作用域是每一个线程之内，每一个请求都是一个新的线程！！！

    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id) {
        threadLocal.set(id);
    }

    public static Long getCurrentId() {
        return threadLocal.get();
    }
}

