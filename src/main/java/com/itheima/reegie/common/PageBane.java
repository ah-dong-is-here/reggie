package com.itheima.reegie.common;

import com.itheima.reegie.entity.Employee;
import org.springframework.stereotype.Component;

import java.util.List;

public class PageBane<T> {
    private List<T> records;
    private  long total;

    @Override
    public String toString() {
        return "PageBane{" +
                "records=" + records +
                ", total=" + total +
                '}';
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public PageBane(List<T> records, long total) {
        this.records = records;
        this.total = total;
    }

    public PageBane() {
    }
}
