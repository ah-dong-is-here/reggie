package com.itheima.reegie.common;

import com.itheima.reegie.service.OperationLogService;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
    @Autowired
    private OperationLogService operationLogService;
    //设置切入点，@Pointcut注解要求配置在方法上方
    @Pointcut("@annotation(com.itheima.reegie.common.SystemLog)")
    private void pt(){}
   /* @Around("pt()")
    public  Object SystemLog(ProceedingJoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();

    }*/

}
