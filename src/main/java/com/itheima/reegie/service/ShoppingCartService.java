package com.itheima.reegie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.entity.ShoppingCart;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ShoppingCartService extends IService<ShoppingCart> {
    //添加购物车
    ShoppingCart shoppingCartAdd(ShoppingCart shoppingCart, HttpServletRequest request);
    //查询购物车
    List<ShoppingCart> selectAllshoppingCart(HttpServletRequest request);
    //清空购物车
    void cleanShoppingCart(HttpServletRequest request);
    //取消购物车
    ShoppingCart subShoppingCart(ShoppingCart shoppingCart, HttpServletRequest request);
}
