package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.entity.Dish;
import com.itheima.reegie.entity.ShoppingCart;
import com.itheima.reegie.mapper.DishMapper;
import com.itheima.reegie.mapper.ShoppingCartMapper;
import com.itheima.reegie.service.ShoppingCartService;
import jdk.nashorn.internal.ir.CallNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private DishMapper dishMapper;
    //添加购物车
    //首先判断此用户的购物车中是否有这个菜品或者套餐，如果购物车中已经有这个菜品或者套餐就在数量上加1，如果是新添加的菜品就直接添加
    @Override
    public ShoppingCart shoppingCartAdd(ShoppingCart shoppingCart, HttpServletRequest request) {
        //这是用户ID
        Long id = (Long) request.getSession().getAttribute("id");
        //这个是菜品ID
        Long dishId = shoppingCart.getDishId();
        //这个是套餐ID
        Long setmealId = shoppingCart.getSetmealId();
        LambdaQueryWrapper<ShoppingCart> shoppingCartLambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (dishId != null) {
            shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId, id).eq(ShoppingCart::getDishId, dishId);
        } else {
            shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId, id).eq(ShoppingCart::getSetmealId, setmealId);
        }
        ShoppingCart shoppingCart1 = shoppingCartMapper.selectOne(shoppingCartLambdaQueryWrapper);
        log.info("+++++" + shoppingCart1);
        if (shoppingCart1 != null) {
            Integer number = shoppingCart1.getNumber();
            shoppingCart1.setNumber(number + 1);
//            Long dishId1 = shoppingCart1.getDishId();
//            Dish dish = dishMapper.selectById(dishId1);
//            BigDecimal price = dish.getPrice();
//            BigDecimal divide = price.divide(BigDecimal.valueOf(100));
//            BigDecimal amount = shoppingCart1.getAmount();
//                BigDecimal add = amount.add(divide);
////            BigDecimal multiply = amount.multiply(BigDecimal.valueOf(number + 1));
//            shoppingCart1.setAmount(add);

            shoppingCartMapper.updateById(shoppingCart1);
            return shoppingCart1;
        } else {
            shoppingCart.setUserId(id);
            shoppingCart.setNumber(1);
            shoppingCartMapper.insert(shoppingCart);
            return shoppingCart;
        }

    }
    //先判断这个菜品在不在购物车中，如果购物车中已经有这个菜品了就更新这个菜品的数量

    //先判断这个套餐在不在购物车中，如果购物车中已经有这个套餐了就更新这个套餐的数量
        /*LambdaQueryWrapper<ShoppingCart> shoppingCartLambdaQueryWrapper = new LambdaQueryWrapper<>();
        shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId, id).eq(ShoppingCart::getDishId, dishId).or().eq(ShoppingCart::getSetmealId, setmealId);
        ShoppingCart shoppingCart2 = shoppingCartMapper.selectOne(shoppingCartLambdaQueryWrapper);
        log.info("====" + shoppingCart2);
        if (shoppingCart2 != null) {
            //获取当前菜品或者套餐中的数量
            Integer number = shoppingCart2.getNumber();
            shoppingCart2.setNumber(number);
            LambdaUpdateWrapper<ShoppingCart> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(ShoppingCart::getId, shoppingCart2.getId());
            shoppingCartMapper.update(shoppingCart2, updateWrapper);
            return shoppingCart2;
        }
        if(shoppingCart2==null){
            shoppingCart.setUserId((Long) request.getSession().getAttribute("id"));
            shoppingCartMapper.insert(shoppingCart);
            ShoppingCart shoppingCart1 = shoppingCartMapper.selectById(shoppingCart.getId());
            return shoppingCart1;
        }*/

       /* LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, id);
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectList(queryWrapper);
        for (ShoppingCart cart : shoppingCartList) {

            if (cart.getDishId().equals(dishId) || cart.getSetmealId().equals(setmealId)) {
                LambdaUpdateWrapper<ShoppingCart> updateWrapper = new LambdaUpdateWrapper<>();//此处可以将这份
                updateWrapper.eq(ShoppingCart::getId, cart.getId()).set(ShoppingCart::getNumber, cart.getNumber() + 1);
                shoppingCartMapper.update(cart, updateWrapper);
                return cart;
            }
        }*/


    //查询购物车

    @Override
    public List<ShoppingCart> selectAllshoppingCart(HttpServletRequest request) {
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        Long id = (Long) request.getSession().getAttribute("id");
        queryWrapper.eq(ShoppingCart::getUserId, id);
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectList(queryWrapper);
        return shoppingCartList;
    }
    //取消购物车
    @Override
    public ShoppingCart subShoppingCart(ShoppingCart shoppingCart, HttpServletRequest request) {
        //这是用户ID
        Long id = (Long) request.getSession().getAttribute("id");
        //这个是菜品ID
        Long dishId = shoppingCart.getDishId();
        //这个是套餐ID
        Long setmealId = shoppingCart.getSetmealId();
        LambdaQueryWrapper<ShoppingCart> shoppingCartLambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (dishId != null) {
            shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId, id).eq(ShoppingCart::getDishId, dishId);
        } else {
            shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId, id).eq(ShoppingCart::getSetmealId, setmealId);
        }
        ShoppingCart shoppingCart1 = shoppingCartMapper.selectOne(shoppingCartLambdaQueryWrapper);
        log.info("+++++" + shoppingCart1);
        Integer number = shoppingCart1.getNumber();
        if(number<=1){
            shoppingCartMapper.deleteById(shoppingCart1.getId());
        }
        shoppingCart1.setNumber(number-1);
        shoppingCartMapper.updateById(shoppingCart1);
        return shoppingCart1;
    }
    //清空购物车
    @Override
    public void cleanShoppingCart(HttpServletRequest request) {
        LambdaQueryWrapper<ShoppingCart> shoppingCartLambdaQueryWrapper = new LambdaQueryWrapper<>();
        Long id = (Long) request.getSession().getAttribute("id");
        shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId, id);
        shoppingCartMapper.delete(shoppingCartLambdaQueryWrapper);
    }
}
