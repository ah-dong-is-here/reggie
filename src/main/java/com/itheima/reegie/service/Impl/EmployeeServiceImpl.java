package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.common.PageBane;
import com.itheima.reegie.entity.Employee;
import com.itheima.reegie.mapper.EmployeeMapper;
import com.itheima.reegie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
//TODO：员工业务层
@Transactional
@Slf4j
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;

    //员工登录
    @Override
    public Employee EmloyeeLongin(Employee employee) {
        //创建条件构造器
        LambdaQueryWrapper<Employee> employeeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //获取密码
        String password = employee.getPassword();
        //Md5加密
        String s = DigestUtils.md5Hex(password);
        employeeLambdaQueryWrapper.eq(Employee::getUsername, employee.getUsername()).eq(Employee::getPassword, s);
        Employee employee1 = employeeMapper.selectOne(employeeLambdaQueryWrapper);
        return employee1;
    }
    //新增员工
    @Override
    public Boolean addEmployee(Employee employee) {
//        employee.setCreateUser();
        String password = "123456";
        String pw = DigestUtils.md5Hex(password);
        employee.setPassword(pw);
        employee.setStatus(1);
        employeeMapper.insert(employee);
        return true;
    }

    //分页查询
    @Override
    public PageBane selectpage(Integer page, Integer pageSize, String name) {
        //创建IPage
        IPage<Employee> employeePage = new Page<>(page, pageSize);
        //模糊查询，
        LambdaQueryWrapper<Employee> employeeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        employeeLambdaQueryWrapper.like(name!=null,Employee::getName,name);
        employeeMapper.selectPage(employeePage, employeeLambdaQueryWrapper);
        //获取相对应的页面数据
        List<Employee> records = employeePage.getRecords();
        //获取总数据的条数
        long total = employeePage.getTotal();
        PageBane<Employee> employeePageBane = new PageBane<>();
        employeePageBane.setRecords(records);
        employeePageBane.setTotal(total);
        return employeePageBane;
    }


    //修改员工状态/编辑员工
    @Override
    public boolean upDateStatus(HttpServletRequest request, Employee employee) {
        //获取Session值，确定修改人
        long id1 = (long) request.getSession().getAttribute("id");

        LambdaUpdateWrapper<Employee> employeeLambdaUpdateWrapper = new LambdaUpdateWrapper<>();

        employee.setUpdateUser(id1);

        employeeLambdaUpdateWrapper.eq(Employee::getId, employee.getId());
      /*  //如果员工状态不等于空
        if (employee.getStatus() != null) {
            employeeLambdaUpdateWrapper.set(Employee::getStatus, employee.getStatus());
        }
        //如果账号不为空
        if (employee.getUsername() != null) {
            employeeLambdaUpdateWrapper.set(Employee::getUsername, employee.getUsername());
        }
        //如果身份证号不为空
        if (employee.getIdNumber() != null) {
            employeeLambdaUpdateWrapper.set(Employee::getIdNumber, employee.getIdNumber());
        }
        //如果员工姓名不为空
        if (employee.getName() != null) {
            employeeLambdaUpdateWrapper.set(Employee::getName, employee.getName());
        }
        //如果手机号不为空
        if (employee.getPhone() != null) {
            employeeLambdaUpdateWrapper.set(Employee::getPhone, employee.getPhone());
        }
        //如果性别不为空
        if (employee.getSex() != null) {
            employeeLambdaUpdateWrapper.set(Employee::getSex, employee.getSex());
        }*/
        int update1 = employeeMapper.update(employee, employeeLambdaUpdateWrapper);
//        boolean update = update(null, employeeLambdaUpdateWrapper);
        return true;
    }

    //数据回显
    @Override
    public Employee selectById(Long id) {
        Employee employee = employeeMapper.selectById(id);
        return employee;
    }
}
