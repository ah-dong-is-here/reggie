package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.Dto.SetmealDto;
import com.itheima.reegie.entity.Category;
import com.itheima.reegie.entity.Setmeal;
import com.itheima.reegie.entity.SetmealDish;
import com.itheima.reegie.mapper.CategoryMapper;
import com.itheima.reegie.mapper.SetmealDishMapper;
import com.itheima.reegie.mapper.SetmealMapper;
import com.itheima.reegie.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//TODO：套餐业务层
@Transactional
//@Service

public class SetmealServiceImpl2 extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    //新增套餐接口
    @Override
    public void saveSetMeal(SetmealDto setmealDto) {
        //调用持久层的方法将套餐的数据放到setmeal表中
        setmealMapper.insert(setmealDto);
        redisTemplate.delete(setmealDto.getCategoryId());
        Long SetMealId = setmealDto.getId();
        ArrayList<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(SetMealId);
            setmealDishMapper.insert(setmealDish);
        }
    }

    //套餐分页查询
    @Override
    public HashMap selectPage(Integer page, Integer pageSize, String name) {

        IPage<Setmeal> setmealPage = new Page<>(page, pageSize);
        //创建模糊查询条件构造器
        LambdaUpdateWrapper<Setmeal> setmealLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        setmealLambdaUpdateWrapper.eq(name != null, Setmeal::getName, name);
        IPage<Setmeal> setmealIPage = setmealMapper.selectPage(setmealPage, setmealLambdaUpdateWrapper);
        //这是信息总条数
        long total = setmealIPage.getTotal();
        //获取页面数据
        List<Setmeal> records = setmealIPage.getRecords();
        ArrayList<SetmealDto> setmealDtos = new ArrayList<>();
        for (Setmeal record : records) {
            SetmealDto setmealDto = new SetmealDto();
            Long categoryId = record.getCategoryId();
            Category category = categoryMapper.selectById(categoryId);
            //类名
            String name1 = category.getName();
            BeanUtils.copyProperties(record, setmealDto);
            setmealDto.setCategoryName(name1);
            setmealDtos.add(setmealDto);
        }
        HashMap<Object, Object> hm = new HashMap<>();

        hm.put("total", total);

        hm.put("records", setmealDtos);
        return hm;
    }

    //删除套餐/包括批量删除
    @Override
    public Boolean deleteByIds(List<Long> ids) {
        List<Setmeal> setmeals = setmealMapper.selectBatchIds(ids);
        for (Setmeal setmeal : setmeals) {
            Integer status = setmeal.getStatus();
            if (status != 0) {
                return false;
            }
        }
        //对套餐表进行逻辑删除/也就是将数据库中的is_deleted字段进行修改将”0“修改为”1“
        //实体类中不存在逻辑删除的is_deleted所对应的属性值，所以就是对套餐类也是直接使用武力删除
       /* for (Long id : ids) {
            LambdaUpdateWrapper<Setmeal> setmealLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
            setmealLambdaUpdateWrapper.eq(Setmeal::getId,id);
            Setmeal setmeal = new Setmeal();
            setmeal.set
            setmealMapper.update()
        }*/
        //对套餐类进行物理删除
        setmealMapper.deleteBatchIds(ids);
        //对套餐菜品类进行物理
        for (Long id : ids) {
            LambdaUpdateWrapper<SetmealDish> setmealDishLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
            setmealDishLambdaUpdateWrapper.eq(SetmealDish::getSetmealId, id);
            setmealDishMapper.delete(setmealDishLambdaUpdateWrapper);
        }
        return true;
    }

    //起售/停售/批量起售/批量停售
    @Override
    public void statusSwitch(Integer statu, List<Long> ids) {
        for (Long id : ids) {
            LambdaUpdateWrapper<Setmeal> setmealLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
            setmealLambdaUpdateWrapper.eq(Setmeal::getId, id);
            Setmeal setmeal = new Setmeal();
            setmeal.setStatus(statu);
            setmealMapper.update(setmeal, setmealLambdaUpdateWrapper);
        }
    }

    //通过ID进行查询套餐数据进行数据回显
    @Override
    public SetmealDto setmealEcho(Long id) {
        //指定返回的数据类型  SetmealDto;
        SetmealDto setmealDto = new SetmealDto();
        //根据id将套餐表里的数据查询出来
        Setmeal setmeal = setmealMapper.selectById(id);
        //将查询出来的套餐数据通过BeanUtils.copyProperties拷贝方法赋值给setmealDto
        BeanUtils.copyProperties(setmeal, setmealDto);
        //创建查询SetmealDish的条件构造器
        LambdaQueryWrapper<SetmealDish> setmealDishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealDishLambdaQueryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> setmealDishes = setmealDishMapper.selectList(setmealDishLambdaQueryWrapper);
        //创建查询分类名的条件构造器
        LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        categoryLambdaQueryWrapper.eq(Category::getId, setmeal.getCategoryId());
        Category category = categoryMapper.selectOne(categoryLambdaQueryWrapper);
        //这是分类名
        String name = category.getName();
        setmealDto.setSetmealDishes((ArrayList<SetmealDish>) setmealDishes);
        setmealDto.setCategoryName(name);
        return setmealDto;
    }
    //将修改过的套餐数据更新到数据库中

    @Override
    public void updateSetmeal(SetmealDto setmealDto) {
        Long id = setmealDto.getId();
        LambdaUpdateWrapper<Setmeal> setmealLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        setmealLambdaUpdateWrapper.eq(Setmeal::getId, id);
        setmealMapper.update(setmealDto, setmealLambdaUpdateWrapper);
        //修改Setmeal_dish表中的数据
        //分为2步：
        //先删除，在添加
        //先将setmeal-dish表中的套餐相关数据进行删除
        ArrayList<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        LambdaQueryWrapper<SetmealDish> setmealDishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealDishLambdaQueryWrapper.eq(SetmealDish::getSetmealId, id);
        setmealDishMapper.delete(setmealDishLambdaQueryWrapper);
        //将新的setmeal-dish表中的数据进行添加
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(id);
            setmealDishMapper.insert(setmealDish);
        }
    }
    //根据套餐分类id查询该套餐分类下的套餐9

    @Override
    public List<Setmeal> selectAllSetmeal(Long categoryId, Integer status) {
      List<Setmeal> setmealList = (List<Setmeal>) redisTemplate.opsForValue().get("Setmeal_" + categoryId);
        if(setmealList!=null &&  setmealList.size()>0){
            return setmealList;
        }
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Setmeal::getCategoryId,categoryId).eq(Setmeal::getStatus,status);
        setmealList = setmealMapper.selectList(queryWrapper);
        redisTemplate.opsForValue().set("Setmeal_"+categoryId,setmealList);
        return setmealList;
    }
}
