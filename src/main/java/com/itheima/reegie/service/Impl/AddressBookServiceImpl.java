package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.common.SystemLog;
import com.itheima.reegie.entity.AddressBook;
import com.itheima.reegie.mapper.AddressBookMapper;
import com.itheima.reegie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Transactional
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {
    @Autowired
    private AddressBookMapper addressBookMapper;

    //用户地址薄功能
    //新增地址
    @SystemLog(description = "新增地址",username = "xiaoli1")
    @Override
    public void addressBook(AddressBook addressBook, HttpServletRequest request) {
        Long id = (Long) request.getSession().getAttribute("id");
        log.info("User_id" + id);
        addressBook.setUserId(id);
        addressBookMapper.insert(addressBook);
    }

    //设置默认地址
    @Override
    public void defaultAdd(AddressBook addressBook) {
        //首先将所有的地址的默认地址值都改成“0”
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getId, addressBook.getId());
        AddressBook addressBook1 = addressBookMapper.selectOne(queryWrapper);
        Long userId = addressBook1.getUserId();
        LambdaUpdateWrapper<AddressBook> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(AddressBook::getUserId, userId).set(AddressBook::getIsDefault, 0);
        addressBookMapper.update(null, updateWrapper);
        //

        //再将前端传来的需要设置为默认地址值改为“1”
        LambdaUpdateWrapper<AddressBook> updateWrapper1 = new LambdaUpdateWrapper<>();
        updateWrapper1.eq(AddressBook::getId,addressBook.getId()).set(AddressBook::getIsDefault,1);
        addressBookMapper.update(null, updateWrapper1);
    }
    //根据ID查询地址

    @Override
    public AddressBook selectAdd(Long id) {
        AddressBook addressBook = addressBookMapper.selectById(id);
        return addressBook;
    }
    //查询默认地址
    @Override
    public AddressBook selectDefault( HttpServletRequest request) {
        //获取当前登录用户ID
        Long phone = (Long) request.getSession().getAttribute("id");
        //默认地址值为1才是默认地址
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(AddressBook::getUserId,phone).eq(AddressBook::getIsDefault,1);

        AddressBook addressBook = addressBookMapper.selectOne(queryWrapper);
        return addressBook;
    }
    //查询指定用户的全部信息

    @Override
    public List<AddressBook> selectUserAllAdd(Long id) {
        LambdaQueryWrapper<AddressBook> addressBookLambdaQueryWrapper = new LambdaQueryWrapper<>();
        addressBookLambdaQueryWrapper.eq(AddressBook::getUserId,id);
        List<AddressBook> addressBookList = addressBookMapper.selectList(addressBookLambdaQueryWrapper);
        return addressBookList;
    }
    //删除地址(可能是批量删除)
    @Override
    public void deleteAdd(List<Long> ids) {
        addressBookMapper.deleteBatchIds(ids);
    }
    //修改地址

    @Override
    public void updateAdd(AddressBook addressBook) {
        addressBookMapper.updateById(addressBook);
    }
}
