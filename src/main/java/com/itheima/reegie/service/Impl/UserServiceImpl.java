package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.entity.User;
import com.itheima.reegie.mapper.UserMapper;
import com.itheima.reegie.service.UserService;
import com.itheima.reegie.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {
    @Autowired
    private  UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    //获取验证码
    @Override
    public void getSendMsg(User user, HttpServletRequest request) {
        //获取前端传来的手机号码
        String phone = user.getPhone();
        //调用生成随机数工具
        Integer integer = ValidateCodeUtils.generateValidateCode(4);
        //清除所有的Session
//        request.getSession().invalidate();

        redisTemplate.opsForValue().set(phone,integer,5, TimeUnit.MINUTES);

        //调用发送短信工具
        //此处不调用，直接略过
        //将手机号码和随机生成的验证码存放到Session中
//        request.getSession().setAttribute(phone,integer);
        log.info("验证码"+integer);
    }
    //用户登录
    @PostMapping("/login")
    @Override
    public boolean userLogin(HashMap map, HttpServletRequest request) {

         //获取Session进行对比
        String phone = String.valueOf(map.get("phone"));

        Object code = map.get("code");
        //获取Session值
//        String attribute = String.valueOf  (request.getSession().getAttribute(phone));
            //
        String attribute = String.valueOf(redisTemplate.opsForValue().get(phone));
        if(attribute.equals(code)){
            redisTemplate.delete(phone);
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone,phone);
            User user = userMapper.selectOne(queryWrapper);
            if(user==null){
                User user1 = new User();
                user1.setPhone(phone);
                userMapper.insert(user1);
                request.getSession().setAttribute("id",user1.getId());
                return true;
            }
            request.getSession().setAttribute("id",user.getId());
            return true;
        }
        //如果对比失败就是登录失败
        return false;
    }
}
