package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.entity.DishFlavor;
import com.itheima.reegie.mapper.DishFlavorMapper;
import com.itheima.reegie.service.DishFlavorService;
import com.itheima.reegie.service.DishService;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}
