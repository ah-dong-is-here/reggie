package com.itheima.reegie.service.Impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.Dto.OrdersDto;
import com.itheima.reegie.common.BaseContext;
import com.itheima.reegie.entity.AddressBook;
import com.itheima.reegie.entity.OrderDetail;
import com.itheima.reegie.entity.Orders;
import com.itheima.reegie.entity.ShoppingCart;
import com.itheima.reegie.mapper.*;
import com.itheima.reegie.service.OrdersService;
import com.itheima.reegie.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private AddressBookMapper addressBookMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private OrderDetailMapper orderDetailMapper;

    //生成订单
    @Override
    public void generateOrder(Orders orders) {
        //前端传来的有 备注  支付方式  地址ID(这个地址ID是默认地址)，
        //通过默认地址可以找到收件人  可以找到地址
        //通过User_Id可以找到相对应的购物车
        //拿到购物车的数据，将其遍历，在遍历的过程中可以将商品的价格进行合计计算

        //通过雪花算法来获取订单Id
        long id = IdWorker.getId();
        //用户User_id
        Long currentId = BaseContext.getCurrentId();
        orders.setUserId(currentId);
        //获取收货人地址
        Long addressBookId = orders.getAddressBookId();
        //获得收获人地址就可以获得收货人的手机号码
        AddressBook addressBook = addressBookMapper.selectById(addressBookId);
        //这是收件人的手机号码
        String phone = addressBook.getPhone();
        //这是收货地址
        String detail = addressBook.getDetail();
        //这是收货人姓名
        String consignee = addressBook.getConsignee();
        //根据User_id查询购物车中的所有的物品，会得到一个List集合，其中集合的长度就是要买物品的数量，
        //创建查询条件构造器
        LambdaQueryWrapper<ShoppingCart> shoppingCarQueryWrapper = new LambdaQueryWrapper<>();
        shoppingCarQueryWrapper.eq(ShoppingCart::getUserId, currentId);
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectList(shoppingCarQueryWrapper);
        //这就是购物车中物品的数量
        //首先制定一个变量来记录这个订单所有的价格
        BigDecimal Amount = null;

        //
        //遍历取得的购物车集合
        //在遍历的过程中要获得所买物品的总的价格
        for (ShoppingCart shoppingCart : shoppingCartList) {
            //创建一个订单详情对象
            OrderDetail orderDetail = new OrderDetail();
            BeanUtils.copyProperties(shoppingCart, orderDetail);
            //orderDetail.setId(id);
            //此物品的数量
            Integer number = shoppingCart.getNumber();
            orderDetail.setNumber(number);
            //此物品的价格
            BigDecimal amount = shoppingCart.getAmount();
            //此物品的总体价格
            Amount = amount.multiply(BigDecimal.valueOf(number));
            orderDetail.setAmount(Amount);
            orderDetail.setOrderId(id);
            orderDetailMapper.insert(orderDetail);
        }
        orders.setCheckoutTime(LocalDateTime.now());
        orders.setOrderTime(LocalDateTime.now());
        orders.setAmount(Amount);
        orders.setId(id);
        orders.setNumber(String.valueOf(id));
        //订单状态展示设置为一个固定值
        orders.setStatus(3);
        orders.setPhone(phone);
        orders.setAddress(detail);
        orders.setConsignee(consignee);
        ordersMapper.insert(orders);
        LambdaQueryWrapper<ShoppingCart> qw1 = new LambdaQueryWrapper<>();
        qw1.eq(ShoppingCart::getUserId, currentId);
        shoppingCartMapper.delete(qw1);
    }

    //分页查询
    @Override
    public Page<OrdersDto> selectOrderPage(Integer page, Integer pageSize) {
        Page<Orders> ordersPage = new Page<>(page, pageSize);
        Page<OrdersDto> orderDetailPage = new Page<>();
        //实现对拷
//        BeanUtils.copyProperties(ordersPage, orderDetailPage, "records");
        //获得用户ID
        Long currentId = BaseContext.getCurrentId();
        //创建条件构造器在订单表中通过查询User_id获取相对应的订单
        LambdaQueryWrapper<Orders> ordersLambdaQueryWrapper = new LambdaQueryWrapper<>();
        ordersLambdaQueryWrapper.eq(Orders::getUserId, currentId);
        ordersLambdaQueryWrapper.orderByDesc(Orders::getOrderTime);
        //查询过程中使用升序的方法对数据进行排序
//        ordersLambdaQueryWrapper.orderByAsc(Orders::getCheckoutTime);
        //通过条件查询，查需订单表中只属于此时用户的的订单并进行升序排序
        Page<Orders> ordersPage1 = ordersMapper.selectPage(ordersPage, ordersLambdaQueryWrapper);
        //实现对拷
        BeanUtils.copyProperties(ordersPage1, orderDetailPage, "records");
        //获得查询出来的数据，这个数据就是前端传来的第？页 ？行的数据
        List<Orders> records = ordersPage1.getRecords();
        //将第122行数据转换成第124行数据
        //因为在一开始
        ArrayList<OrdersDto> ordersDtos = new ArrayList<>();
        //将records进行遍历，获得第一行数据
        for (Orders record : records) {
            OrdersDto ordersDto = new OrdersDto();
            BeanUtils.copyProperties(record, ordersDto);
            Long id = record.getId();
            LambdaQueryWrapper<OrderDetail> orderDetailLambdaQueryWrapper = new LambdaQueryWrapper<>();
            orderDetailLambdaQueryWrapper.eq(OrderDetail::getOrderId, id);
            List<OrderDetail> orderDetails = orderDetailMapper.selectList(orderDetailLambdaQueryWrapper);
            ordersDto.setOrderDetails(orderDetails);
            ordersDto.setUserName(record.getUserName());
            ordersDto.setAddress(record.getAddress());
            ordersDto.setPhone(record.getPhone());
            ordersDtos.add(ordersDto);
        }
        orderDetailPage.setRecords(ordersDtos);
        return orderDetailPage;


    }

    //查询订单(管理端)
    @Override
    public Page selectOrdersPAge(HashMap hashMap) {
        //页数
        String page1 = (String) hashMap.get("page");
        Integer page = Integer.valueOf(page1);
        //数据条数
        String pageSize1 = (String) hashMap.get("pageSize");
        Integer pageSize = Integer.valueOf(pageSize1);
        //订单号

        //开始时间

        LocalDateTime beginTime = null;
        LocalDateTime endTime = null;
        if (hashMap.get("beginTime") != null && hashMap.get("endTime") != null) {
            String beginTime1 = (String) hashMap.get("beginTime");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime beginTime2 = LocalDateTime.parse(beginTime1, formatter);
            beginTime = beginTime2;
            String endTime1 = (String) hashMap.get("endTime");
            LocalDateTime endTime2 = LocalDateTime.parse(endTime1, formatter);
            endTime = endTime2;
        }

        //结束时间

        //创建一个Page类
        Page<Orders> ordersPage = new Page<>(page, pageSize);
        log.info("第一个分页查询数据" + ordersPage);
        //创建条件构造器

        LambdaQueryWrapper<Orders> orderLambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (hashMap.get("number")!=null) {
            orderLambdaQueryWrapper.eq(hashMap.get("number") != null, Orders::getNumber, Long.valueOf(String.valueOf(hashMap.get("number"))));
        }
        orderLambdaQueryWrapper.between(beginTime != null, Orders::getCheckoutTime, beginTime, endTime);
        Page<Orders> ordersPage1 = ordersMapper.selectPage(ordersPage, orderLambdaQueryWrapper);
        log.info("查询的分页数据" + ordersPage1);
        return ordersPage1;
    }


    @Override
    public void getUserData(HttpServletResponse response) throws IOException {
        List<Orders> ordersList = ordersMapper.selectList(null);
        log.info("查询出来的Order数据库中的数据"+ordersList);
        ServletOutputStream outputStream = response.getOutputStream();
        Integer integer = ValidateCodeUtils.generateValidateCode(4);
        String filename = "C:\\Users\\hxd\\Desktop\\Order"+integer+".xlsx";
        // 向Excel中写入数据 也可以通过 head(Class<?>) 指定数据模板
        response.setHeader("Content-disposition","订单明细.xlsx");
        EasyExcel.write(filename,Orders.class)
                .sheet("订单信息")
                .doWrite(ordersList);

        IOUtils.copy(new FileInputStream(filename),outputStream);
        outputStream.close();

    }
}
