package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.entity.SetmealDish;
import com.itheima.reegie.mapper.SetmealDishMapper;
import com.itheima.reegie.service.SetmealDishService;
import com.itheima.reegie.service.SetmealService;
import org.springframework.stereotype.Service;

@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper,SetmealDish> implements SetmealDishService {
}
