package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.Dto.DishDto;
import com.itheima.reegie.entity.Category;
import com.itheima.reegie.entity.Dish;
import com.itheima.reegie.entity.DishFlavor;
import com.itheima.reegie.mapper.CategoryMapper;
import com.itheima.reegie.mapper.DishFlavorMapper;
import com.itheima.reegie.mapper.DishMapper;
import com.itheima.reegie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//TODO：菜品业务层
@Transactional
@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorMapper dishFlavorMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    //添加菜品
    @Override
    public void insert(DishDto dishDto) {
        redisTemplate.delete("dish_"+dishDto.getCategoryId());
        //调用持久层的方法将dish数据添加入表中
        dishMapper.insert(dishDto);

        Long id = dishDto.getId();
        log.info("=========菜品id：" + id);
        List<DishFlavor> flavors = dishDto.getFlavors();
        for (DishFlavor flavor : flavors) {
            flavor.setDishId(id);
            dishFlavorMapper.insert(flavor);
        }
        //flavors.get(0).setDishId(id);
    }

    //分页查询菜品数据
    @Override
    public HashMap selectPage(Integer page, Integer pageSize, String name) {
        IPage<Dish> dishPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.like(name != null, Dish::getName, name);
        IPage<Dish> iPage = dishMapper.selectPage(dishPage, dishLambdaQueryWrapper);
        List<Dish> r = iPage.getRecords();

        ArrayList<DishDto> dishDtos = new ArrayList<>();
        for (Dish dish : r) {
            Long categoryId = dish.getCategoryId();
            Category category = categoryMapper.selectById(categoryId);
            //这就是菜品的类名
            String name1 = category.getName();
            DishDto dishDto = new DishDto();
            dishDto.setCategoryName(name1);
            BeanUtils.copyProperties(dish, dishDto);
            dishDtos.add(dishDto);
        }
        HashMap<Object, Object> mp = new HashMap<>();
        mp.put("records", dishDtos);
        mp.put("total", iPage.getTotal());
        return mp;
    }

    //修改菜品时用作数据回显
    @Override
    public DishDto selectById(Long id) {
        //创建口味表的查询条件构造器
        LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishFlavorLambdaQueryWrapper.eq(DishFlavor::getDishId, id);
        //通过以DishId作为查询条件查询出在口味表中的数据封装到集合中去
        List<DishFlavor> dishFlavors = dishFlavorMapper.selectList(dishFlavorLambdaQueryWrapper);
        //根据前端传过来的ID数据在Dish表中进行查询
        Dish dish = dishMapper.selectById(id);
        //获取在Dish表中的对象的CategoryId到Category表中去进行查询
        Category category = categoryMapper.selectById(dish.getCategoryId());
        DishDto dishDto = new DishDto();
        dishDto.setCategoryName(category.getName());
        dishDto.setFlavors(dishFlavors);
        BeanUtils.copyProperties(dish, dishDto);
        return dishDto;
    }

    //正式修改数据
    @Override
    public void updateDish(DishDto dishDto) {
        //创建Dish表中需要修改的条件构造器
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(Dish::getId, dishDto.getId());
        //根据Id在Dish表中进行修改
        dishMapper.update(dishDto, dishLambdaQueryWrapper);
        //获取口味数据
        List<DishFlavor> flavors = dishDto.getFlavors();
        //创建DishFlavors表中的数据
        for (DishFlavor flavor : flavors) {//此处
            LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
            //先将原先口味表中的数据根据菜品id进行删除
            dishFlavorLambdaQueryWrapper.eq(DishFlavor::getDishId, dishDto.getId());//flavor.getDishId()
            System.out.println("菜品ID：" + flavor.getDishId());

            dishFlavorMapper.delete(dishFlavorLambdaQueryWrapper);
            Long dishId = flavor.getDishId();
            System.out.println(dishId);
            //再将修改好的数据添加到口味表中
            flavor.setDishId(dishDto.getId());
            dishFlavorMapper.insert(flavor);
        }
    }

    //删除/批量删除
    @Override
    public void deleteByIds(List<Long> ids) {
        for (Long id : ids) {
            LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
            dishFlavorLambdaQueryWrapper.eq(DishFlavor::getDishId, id);
            dishFlavorMapper.delete(dishFlavorLambdaQueryWrapper);
        }
        dishMapper.deleteBatchIds(ids);
    }

    //起售/停售/批量起售/批量停售
    @Override
    public void statusSwitch(Integer statu, List<Long> ids) {
        for (Long id : ids) {
            LambdaUpdateWrapper<Dish> dishLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
            dishLambdaUpdateWrapper.eq(Dish::getId, id);
            Dish dish = new Dish();
            dish.setStatus(statu);
            dishMapper.update(dish, dishLambdaUpdateWrapper);
        }
    }

    //通过查询获得了该分类下的所有菜品集合
    @Override
    public List<DishDto> selectByCategoryId(Long categoryId, Integer status) {
        List<DishDto> dishDtos = (List<DishDto>) redisTemplate.opsForValue().get("dish_"+categoryId);
        if(dishDtos!=null && dishDtos.size()>0 ){
            return dishDtos;
        }
        //创建条件构造器
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();

        //select * from dish  where Category_id=?
        dishLambdaQueryWrapper.eq(Dish::getCategoryId, categoryId).eq(Dish::getStatus, status);
        //通过查询获得了该分类下的所有菜品集合

        List<Dish> dishList = dishMapper.selectList(dishLambdaQueryWrapper);
        //创建DishDto集合，将每一个菜品及其口都做一个DishDto对象存放进去
        dishDtos = new ArrayList<>();
        for (Dish dish : dishList) {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(dish, dishDto);
            //DishDto
            LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(DishFlavor::getDishId, dish.getId());
            //每一个菜品的多个口味
            List<DishFlavor> dishFlavors = dishFlavorMapper.selectList(lambdaQueryWrapper);
            dishDto.setFlavors(dishFlavors);
            dishDtos.add(dishDto);
        }
        redisTemplate.opsForValue().set("dish_"+categoryId,dishDtos);
        //将获得的dish集合返回到表现层
            return dishDtos;
    }
}
