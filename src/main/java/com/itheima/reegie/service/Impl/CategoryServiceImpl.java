package com.itheima.reegie.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reegie.common.PageBane;
import com.itheima.reegie.entity.Category;
import com.itheima.reegie.entity.Dish;
import com.itheima.reegie.entity.Employee;
import com.itheima.reegie.entity.Setmeal;
import com.itheima.reegie.mapper.CategoryMapper;
import com.itheima.reegie.mapper.DishMapper;
import com.itheima.reegie.mapper.SetmealMapper;
import com.itheima.reegie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

//TODO：菜品分类业务层
@Slf4j
@Transactional
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;


    //添加菜品

    @Override
    public boolean addCategory(Category category) {
        //调用持久层的insert方法进行添加
        //TODO：提示：菜品名在数据库中是唯一，所以如果添加的类名如果重复是会报错，这里还没有处理
        categoryMapper.insert(category);

        return true;
    }

    //分页分类查询
    @Override
    public IPage<Category> selectpage(Integer page, Integer pageSize) {
        //创建IPage
        IPage<Category> categoryPage = new Page<>(page, pageSize);
        //调用持久层的SelectPage方法进行分页查询，并存储到categoryPage中
        categoryMapper.selectPage(categoryPage, null);
        //将categoryPage返回到表现层
        return categoryPage;
    }

    //删除分类
    @Override
    public boolean selectByid(Long id) {
        //创建当前分类中查询Dish中是否包含有对应的菜品的条件构造器
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(Dish::getCategoryId, id);
        List<Dish> dishes = dishMapper.selectList(dishLambdaQueryWrapper);
        System.out.println(dishes);
        //创建当前分类中查询Dish中是否包含有对应的菜品的条件构造器
        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealLambdaQueryWrapper.eq(Setmeal::getCategoryId, id);
        List<Setmeal> setmeals = setmealMapper.selectList(setmealLambdaQueryWrapper);
        System.out.println(setmeals);
        if (dishes.size() > 0) {
            return false;
        }if(setmeals.size()>0){
            return false;
        }
        return true;
    }

    @Override
    public void deleteById(Long id) {
      categoryMapper.deleteById(id);
    }
    //修改分类
    @Override
    public boolean updateCategory(Category category) {
        LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        categoryLambdaQueryWrapper.eq(Category::getId,category.getId());
        int update = categoryMapper.update(category, categoryLambdaQueryWrapper);
        return true;
    }

    @Override
    public List<Category> selectByType(Integer type) {
        //创建条件构造器，将表现层传下来的数据作为条件放到里面
        LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        categoryLambdaQueryWrapper.eq(type!=null,Category::getType,type);
        //调用Category持久层的方法将type为1的菜系类别分装到集合中返回到表现层
        List<Category> categories = categoryMapper.selectList(categoryLambdaQueryWrapper);
        return categories;

    }

    @Override
    public List<Category> selectAllcategory() {
        List<Category> categories1 = categoryMapper.selectList(null);
        return categories1;
    }
}
