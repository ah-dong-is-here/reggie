package com.itheima.reegie.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.Dto.DishDto;
import com.itheima.reegie.entity.Dish;

import java.util.HashMap;
import java.util.List;

public interface DishService extends IService<Dish> {
    //添加菜品
    void insert(DishDto dishDto);
    //分页查询
    HashMap selectPage(Integer page, Integer pageSize, String name);
    //修改菜品时用作数据回显
    DishDto selectById(Long id);
    //修改菜品
    void updateDish(DishDto dishDto);
    //删除及批量删除
    void deleteByIds(List<Long> ids);
    //起售/停售/批量起售/批量停售
    void statusSwitch(Integer statu, List<Long> ids);
    //根据分类id查询该份类下的所有菜品集合
    List<DishDto> selectByCategoryId(Long categoryId,Integer status);
}
