package com.itheima.reegie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.entity.AddressBook;
import com.itheima.reegie.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public interface UserService extends IService<User> {
    //获取验证码请求
    void getSendMsg(User user, HttpServletRequest request);
    //输入验证码，进行登录
    boolean userLogin(HashMap map,HttpServletRequest request);
}
