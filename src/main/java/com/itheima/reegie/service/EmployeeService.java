package com.itheima.reegie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.common.PageBane;
import com.itheima.reegie.entity.Employee;

import javax.servlet.http.HttpServletRequest;

public interface EmployeeService extends IService<Employee> {
    //员工登录
    Employee EmloyeeLongin(Employee employee);

    //新增员工
    Boolean addEmployee(Employee employee);

    //分页查询
    PageBane selectpage(Integer page, Integer pageSize, String name);

    //修改员工状态/编辑员工
    boolean upDateStatus(HttpServletRequest request,Employee employee);

    //数据回显
    Employee selectById(Long id);
}
