package com.itheima.reegie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.Dto.SetmealDto;
import com.itheima.reegie.entity.Setmeal;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

public interface SetmealService extends IService<Setmeal> {
    //新增套餐接口
    void saveSetMeal(SetmealDto setmealDto);

    //套餐分页查询
    HashMap selectPage(Integer page, Integer pageSize, String name);

    //删除套餐
    Boolean deleteByIds(List<Long> ids);
    //起售/停售/批量起售/批量停售
    void statusSwitch(Integer statu, List<Long> ids);
    //通过ID进行查询套餐数据进行数据回显
    SetmealDto setmealEcho(Long id);
    //正式将修改过的数据更新到数据库
    void updateSetmeal(SetmealDto setmealDto);
    //根据套餐分类id查询该套餐分类下的套餐
    List<Setmeal> selectAllSetmeal(Long categoryId, Integer status);
}
