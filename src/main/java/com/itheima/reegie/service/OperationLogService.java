package com.itheima.reegie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.entity.OperationLog;

public interface OperationLogService extends IService<OperationLog> {
}
