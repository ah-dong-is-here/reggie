package com.itheima.reegie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.entity.AddressBook;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AddressBookService extends IService<AddressBook> {
    //用户地址薄功能
    //新增地址
    void addressBook(AddressBook addressBook, HttpServletRequest request);

    //设置默认地址
    void defaultAdd(AddressBook addressBook);

    //根据ID查询地址
    AddressBook selectAdd(Long id);

    //查询默认地址
    AddressBook selectDefault(HttpServletRequest request);
    //查询指定用户的全部地址
    List<AddressBook> selectUserAllAdd(Long id);
    //删除地址(批量删除)
    void deleteAdd(List<Long> ids);
    //修改地址
    void updateAdd(AddressBook addressBook);
}
