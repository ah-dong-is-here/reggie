package com.itheima.reegie.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.common.PageBane;
import com.itheima.reegie.entity.Category;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface CategoryService extends IService<Category> {
    //新增分类
    boolean addCategory(Category category);

    //分页查询
    IPage<Category> selectpage(Integer page, Integer pageSize);

    //TODO：删除分类
    //先判断此分类中是否有菜品
    boolean selectByid(Long id);
    //如果分类中没有菜品，则根据菜品进行删除
    void deleteById(Long id);

    boolean updateCategory(Category category);

    List<Category> selectByType(Integer type);

    List<Category> selectAllcategory();

}

