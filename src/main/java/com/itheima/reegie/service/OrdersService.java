package com.itheima.reegie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.Dto.OrdersDto;
import com.itheima.reegie.entity.Orders;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public interface OrdersService extends IService<Orders> {
    void generateOrder(Orders orders);

    //分页查询
    Page<OrdersDto> selectOrderPage(Integer page, Integer pageSize);
    //查询订单接口(管理端)
    Page selectOrdersPAge(HashMap hashMap);
    //订单明细中新增一个Excel导出功能
    void getUserData(HttpServletResponse response) throws IOException;


}
