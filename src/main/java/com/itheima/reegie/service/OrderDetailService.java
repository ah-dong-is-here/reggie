package com.itheima.reegie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reegie.entity.OrderDetail;

public interface OrderDetailService  extends IService<OrderDetail> {
}
