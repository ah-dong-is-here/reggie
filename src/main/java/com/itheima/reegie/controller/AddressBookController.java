package com.itheima.reegie.controller;

import com.itheima.reegie.common.R;
import com.itheima.reegie.entity.AddressBook;
import com.itheima.reegie.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/addressBook")
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;

    //用户地址薄功能
    //新增地址
    @PostMapping
    public R<String> addressBook(@RequestBody AddressBook addressBook, HttpServletRequest request) {
        addressBookService.addressBook(addressBook, request);
        return R.success("保存地址成功");
    }

    //设置默认地址
    //设置默认地址
    @PutMapping("/default")
    public R<String> defaultAdd(@RequestBody AddressBook addressBook) {
        addressBookService.defaultAdd(addressBook);
        return R.success("设置成功");
    }

    //根据ID查询地址
    @GetMapping("/{id}")
    public R<AddressBook> selectAdd(@PathVariable Long id) {

        AddressBook addressBook = addressBookService.selectAdd(id);

        return R.success(addressBook);
    }

    //查询默认地址
    @GetMapping("/default")
    public R<AddressBook> selectDefaultAdd(HttpServletRequest request) {
        AddressBook addressBook = addressBookService.selectDefault(request);
        return R.success(addressBook);

    }

    //查询指定用户的全部地址
    @GetMapping("/list")
    public R<List<AddressBook>> selectUserAllAdd(HttpServletRequest request) {
        Long id = (Long) request.getSession().getAttribute("id");
        List<AddressBook> addressBookList = addressBookService.selectUserAllAdd(id);
        return R.success(addressBookList);

    }
    //删除地址(批量删除)
    @DeleteMapping
    public  R<String> deleteAdd(@RequestParam List<Long> ids){
        addressBookService.deleteAdd(ids);
        return  R.success("删除成功");
    }
    //修改地址
    @PutMapping
    public R<String> updateAdd(@RequestBody AddressBook addressBook){
        //使用AddressBook类来接受对象
        addressBookService.updateAdd(addressBook);
        return R.success("修改成功");
    }
}
