package com.itheima.reegie.controller;

import com.itheima.reegie.common.BaseContext;
import com.itheima.reegie.common.PageBane;
import com.itheima.reegie.common.R;
import com.itheima.reegie.config.MyMetaObjectHandler;
import com.itheima.reegie.entity.Employee;
import com.itheima.reegie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private MyMetaObjectHandler myMetaObjectHandler;

    //登录
    @PostMapping("/login")
    public R EmployeeLogin(HttpServletRequest request, @RequestBody Employee employee) {
        //创建Session对象
        HttpSession session = request.getSession();
        //调用Service层中的登录方法
        Employee employee1 = employeeService.EmloyeeLongin(employee);
        //session.setAttribute("employee",employee1);
        //

            session.setAttribute("id", employee1.getId());
        if (employee1 != null) {
            return R.success(employee);
        }
        return R.error("登录失败");
    }

    //TODO：退出登录
       //退出登录就是清空Session
          //两种清空方式
             //session.invalidate();
             //session.removeAttribute("employee");
    @PostMapping("/logout")
    public R LoginOut(HttpServletRequest request) {
        HttpSession session = request.getSession();
        //session.invalidate();
        log.info("logout -----");
        //清除Session
        session.removeAttribute("id");
        return R.success(null);
    }

    //新增员工
    @PostMapping
    public R AddEmployee(HttpServletRequest request, @RequestBody Employee employee) {
//        HttpSession session = request.getSession();
//        Employee employee1 = (Employee) session.getAttribute("employee");
        Boolean result = employeeService.addEmployee(employee);
        if (result) {
            return R.success(null);
        }
        return R.error("添加失败");
    }

    //分页查询
    @GetMapping("/page")
    public R selectPage(Integer page,Integer pageSize,String name) {
        PageBane<Employee> pageBane = employeeService.selectpage(page, pageSize,name);
        return R.success(pageBane);
    }

    //修改员工状态/编辑员工
    @PutMapping
    public R upDateStatus( @RequestBody Employee employee,HttpServletRequest request) {
        boolean result = employeeService.upDateStatus(request,employee);
        if (result) {
            return R.success("修改成功");
        }
        return R.error("修改失败");
    }

    //数据回显
    @GetMapping("/{id}")
    public R upDateEmployee(@PathVariable Long id) {
        Employee employee = employeeService.selectById(id);
        return R.success(employee);
    }
}
