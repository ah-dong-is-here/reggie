package com.itheima.reegie.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.itheima.reegie.Dto.DishDto;
import com.itheima.reegie.common.R;
import com.itheima.reegie.entity.Dish;
import com.itheima.reegie.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/dish")
public class DishController {
    @Autowired
    private DishService dishService;

    //添加菜品
    @PostMapping
    public R saveDish(@RequestBody DishDto dishDto) {
        dishService.insert(dishDto);
        return R.success("添加成功");
    }

    //分页查询菜品数据
    @GetMapping("/page")
    public R selectPage(Integer page, Integer pageSize, String name) {
        HashMap hashMap = dishService.selectPage(page, pageSize, name);
        return R.success(hashMap);
    }

    //修改菜品数据
    //接口一：根据id查询菜品数据 用于回显
    @GetMapping("/{id}")
    //回显英语单词==>echo
    public R dishEcho(@PathVariable Long id) {
        DishDto dishDto = dishService.selectById(id);
        return R.success(dishDto);
    }

    //接口二：正式将修改好的数据进行存储
    @PutMapping
    public R updateDish(@RequestBody DishDto dishDto) {
        dishService.updateDish(dishDto);
        return R.success("修改成功");
    }
    //删除菜品及批量删除
    @DeleteMapping
    public R deleteByIds(@RequestParam List<Long> ids){
        dishService.deleteByIds(ids);
        return R.success("删除成功");
    }
    //起售/停售/批量起售/批量停售
    @PostMapping("/status/{statu}")
    public R statusSwitch(@PathVariable Integer statu, @RequestParam List<Long> ids){
        dishService.statusSwitch(statu,ids);
        return R.success("操作成功");
    }
    //根据分类ID查询分类下的所有菜品集合
    @GetMapping("/list")
    public R dishList(Long categoryId,Integer status){
        List<DishDto> dishDtoList=dishService.selectByCategoryId(categoryId,status);
        return R.success(dishDtoList);
    }
}
