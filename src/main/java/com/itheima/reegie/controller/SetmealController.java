package com.itheima.reegie.controller;

import com.itheima.reegie.Dto.SetmealDto;
import com.itheima.reegie.common.R;
import com.itheima.reegie.entity.Setmeal;
import com.itheima.reegie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    //新增套餐接口
    @PostMapping
    public R addSetMeal(@RequestBody SetmealDto setmealDto) {
        //调用业务层的方法将前端传过来的套餐进行保存
        setmealService.saveSetMeal(setmealDto);
        return R.success("保存套餐成功");
    }

    //套餐分页查询
    @GetMapping("/page")
    public R selectPage(Integer page, Integer pageSize, String name) {
        HashMap hashMap = setmealService.selectPage(page, pageSize, name);
        return R.success(hashMap);
    }

    //删除套餐/同时也包含批量删除
    @DeleteMapping
    public R deleteByIds(@RequestParam List<Long> ids) {
        Boolean aBoolean = setmealService.deleteByIds(ids);
        if (aBoolean) {
            return R.success("套餐删除成功");
        }
        return R.error("当前套餐在售，不可删除");
    }

    //起售/停售/批量起售/批量停售
    @PostMapping("/status/{statu}")
    public R statusSwitch(@PathVariable Integer statu, @RequestParam List<Long> ids) {
        setmealService.statusSwitch(statu, ids);
        return R.success("操作成功");
    }

    //修改套餐
    //修改套餐接口1：根据套餐Id查询套餐数据(用作回显数据)
    @GetMapping("/{id}")
    //回显英语单词==>echo
    private R setMealEcho(@PathVariable Long id) {
        SetmealDto setmealDto = setmealService.setmealEcho(id);
        return R.success(setmealDto);
    }

    //修改套餐接口2：正式修改套餐
    @PutMapping
    public R updateSetmeal(@RequestBody SetmealDto setmealDto) {
        setmealService.updateSetmeal(setmealDto);
        return R.success("修改套餐失败");

    }

    @GetMapping("/list")
    public R<List<Setmeal>> selectAllSetmeals(Long categoryId, Integer status) {
        List<Setmeal> setmealList = setmealService.selectAllSetmeal(categoryId,status);
        return R.success(setmealList);
    }
}
