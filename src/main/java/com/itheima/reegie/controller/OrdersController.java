package com.itheima.reegie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reegie.Dto.OrdersDto;
import com.itheima.reegie.common.R;
import com.itheima.reegie.common.SystemLog;
import com.itheima.reegie.entity.Orders;
import com.itheima.reegie.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    //生成订单
    @SystemLog(description = "生成订单",username = "小李")
    @PostMapping("/submit")
    public R<String> generateOrder(@RequestBody Orders orders) {
        ordersService.generateOrder(orders);
        return R.success("清空购物车成功");
    }

    //订单分页查询
    @GetMapping("/userPage")
    private R orderPage(Integer page, Integer pageSize) {
        Page<OrdersDto> ordersPage = ordersService.selectOrderPage(page, pageSize);
        return R.success(ordersPage);
    }

    //查询订单(管理管)

    @GetMapping("/page")
    private R selectOrdersPAge(@RequestParam HashMap hashMap) {
        Page page = ordersService.selectOrdersPAge(hashMap);
        return R.success(page);
    }
    //Excel到处功能

    @GetMapping("/export")
    public void ecportExcel(HttpServletResponse response) throws IOException {
        ordersService.getUserData(response);

    }
    //修改订单状态
    @PutMapping
    private R updateStatus(){
        return R.success(1);
    }
}
