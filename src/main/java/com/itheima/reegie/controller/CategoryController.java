package com.itheima.reegie.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.itheima.reegie.common.R;
import com.itheima.reegie.entity.Category;
import com.itheima.reegie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    //新增菜品/套餐分类
    @PostMapping

    public R addCategory(@RequestBody Category category) {

        boolean result = categoryService.addCategory(category);

        if (result) {

            return R.success(null);
        }

        return R.error("添加失败");

    }

    //分页分类查询
    @GetMapping("/page")
    public R selectPage(Integer page, Integer pageSize) {
        IPage<Category> categoryIPage = categoryService.selectpage(page, pageSize);
        return R.success(categoryIPage);
    }

    //删除分类
    @DeleteMapping
    public R deleteByid(Long id) {

        boolean result = categoryService.selectByid(id);
        if (result) {
            categoryService.deleteById(id);
            return R.success("删除成功");
        }
        return R.error("当前分类下有套餐或者菜品，不能删除");
    }

    //修改分类
    @PutMapping
    public R upDate(@RequestBody Category category) {
        boolean result = categoryService.updateCategory(category);
        return R.success("修改成功");
    }
    //查询菜品分类列表数据
    @GetMapping("/list")
    public R selectByType(Integer type) {
        //调用业务层的dishService方法去查询，并返回Category类型的集合
        List<Category> categorylist=categoryService.selectByType(type);
      log.info("========================"+categorylist);
        return R.success(categorylist);
    }
  /*  @GetMapping("/list")
    public R selectAllcategory() {
        //调用业务层的dishService方法去查询，并返回Category类型的集合
        List<Category> categorylist1=categoryService.selectAllcategory();
        log.info("所有菜品分类"+categorylist1);
        return R.success(categorylist1);
    }*/
}
