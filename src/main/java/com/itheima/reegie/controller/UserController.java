package com.itheima.reegie.controller;

import com.itheima.reegie.common.R;
import com.itheima.reegie.entity.User;
import com.itheima.reegie.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    //获取验证码
    @PostMapping("sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpServletRequest request){
        //前端传来获取验证码请求
        //携带手机号码过来
        userService.getSendMsg(user,request);

        return R.success("验证码发送成功");
    }
    //用户输入验证码，进行登录
    @PostMapping("/login")
    public R<String>  userLogin(@RequestBody HashMap map,HttpServletRequest request){
        boolean b = userService.userLogin(map, request);
        if(b){
            return R.success("登录成功");
        }
        return R.error("手机号或者验证码有误");
    }
    //用户退出登录
    @PostMapping("/loginout")
    public  R<String>  userLoginOut(HttpServletRequest request){
//        request.getSession().removeAttribute("id");
        request.getSession().invalidate();
        return R.success("退出成功");
    }

}
