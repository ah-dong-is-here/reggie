package com.itheima.reegie.controller;

import com.itheima.reegie.common.R;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

/*@RestController
@RequestMapping("/common")*/
public class ImagesCommon {


    //图片上传
    @PostMapping("/upload")
    public R ImagesCommon(MultipartFile file) throws IOException {
        //获取文件名
        String originalFilename = file.getOriginalFilename();
        //获取文件后缀名

        String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
        //使用uuid对文件名进行更改，从而确定唯一性
        String newName = UUID.randomUUID().toString() + substring;

        file.transferTo(new File("G://2023ITheimaReggie//reggie_take_out//src//main//resources//backend//images/" + newName));
        return R.success(newName);
    }

    //图片下载
    @GetMapping("download")
    public void download(String name, HttpServletResponse response) throws IOException {
        ServletOutputStream output = response.getOutputStream();
        IOUtils.copy(new FileInputStream(
                "G://2023ITheimaReggie//reggie_take_out//src//main//resources//backend//images/" + name), output);

    }

}
