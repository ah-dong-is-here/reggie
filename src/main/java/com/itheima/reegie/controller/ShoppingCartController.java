package com.itheima.reegie.controller;

import com.itheima.reegie.common.R;
import com.itheima.reegie.entity.ShoppingCart;
import com.itheima.reegie.service.ShoppingCartService;
import com.sun.org.apache.xpath.internal.objects.XString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    //添加购物车
    @PostMapping("/add")
    public R<ShoppingCart> shoppingCartAdd(@RequestBody ShoppingCart shoppingCart, HttpServletRequest request) {
        ShoppingCart shoppingCart1 = shoppingCartService.shoppingCartAdd(shoppingCart, request);
        return R.success(shoppingCart1);
    }

    //查询购物车
    @GetMapping("/list")
    public R<List<ShoppingCart>> selectAllShoppingCart(HttpServletRequest request) {
        List<ShoppingCart> shoppingCartList = shoppingCartService.selectAllshoppingCart(request);
        return R.success(shoppingCartList);
    }

    // 清空购物车
    @DeleteMapping("/clean")
    public R<String> cleanShoppingCart(HttpServletRequest request) {
        shoppingCartService.cleanShoppingCart(request);
        return R.success("清空购物车成功");
    }

    // 取消购物车
    @PostMapping("/sub")
    public R<ShoppingCart> subShoppingCart(@RequestBody ShoppingCart shoppingCart, HttpServletRequest request) {
        ShoppingCart shoppingCart1 = shoppingCartService.subShoppingCart(shoppingCart, request);
        return R.success(shoppingCart1);

    }
}
