package com.itheima.reegie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reegie.entity.OperationLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OperationLogMapper extends BaseMapper<OperationLog> {
}
