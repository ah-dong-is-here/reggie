package com.itheima.reegie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reegie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
//TODO：员工持久层
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

}
