package com.itheima.reegie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reegie.entity.Category;
import org.apache.ibatis.annotations.Mapper;
//TODO：菜品分类持久层
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
