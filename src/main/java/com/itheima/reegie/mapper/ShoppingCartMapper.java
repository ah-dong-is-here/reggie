package com.itheima.reegie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reegie.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {
}
