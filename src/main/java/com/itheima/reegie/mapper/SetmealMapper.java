package com.itheima.reegie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reegie.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;
//TODO:套餐持久层
@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
