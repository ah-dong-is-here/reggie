package com.itheima.reegie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reegie.entity.Dish;
import org.apache.ibatis.annotations.Mapper;
//TODO：菜品持久层
@Mapper
public interface  DishMapper extends BaseMapper<Dish> {

}
