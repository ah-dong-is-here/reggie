package com.itheima.reegie.Dto;

import com.itheima.reegie.entity.OrderDetail;
import com.itheima.reegie.entity.Orders;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class OrdersDto extends Orders {
    private String userName;

    private String phone;

    private String address;

    private String consignee;

    private List<OrderDetail> orderDetails;
}
