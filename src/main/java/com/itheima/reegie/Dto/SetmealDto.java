package com.itheima.reegie.Dto;

import com.itheima.reegie.entity.Setmeal;
import com.itheima.reegie.entity.SetmealDish;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

     private ArrayList<SetmealDish>  setmealDishes;

     private String categoryName;
}
