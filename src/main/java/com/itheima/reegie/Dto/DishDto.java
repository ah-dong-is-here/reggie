package com.itheima.reegie.Dto;

import com.itheima.reegie.entity.Dish;
import com.itheima.reegie.entity.DishFlavor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DishDto extends Dish  {
    private static final long serialVersionUID = 2L;

    private List<DishFlavor> flavors = new ArrayList<>();

    private String categoryName;

    private Integer copies;
}
