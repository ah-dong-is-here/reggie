package com.itheima.reegie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OperationLog {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private  Long id;

    //防止精度缺失注解
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private  Long userId;

    private String  action;

    @JsonFormat(locale = "zh",timezone = "GMT+8",pattern ="yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime  operationTime;
}
