package com.itheima.reegie.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.itheima.reegie.common.LocalDateStringConver;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Orders implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonFormat(shape = JsonFormat.Shape.STRING)

    private Long id;

    //订单号
    @ExcelProperty(value = "订单号")
    private String number;

    //订单状态 1待付款，2待派送，3已派送，4已完成，5已取消
    @ExcelProperty(value = "订单状态")
    private Integer status;


    //下单用户id
    @TableField(fill = FieldFill.INSERT)
    private Long userId;

    //
    @TableField(fill = FieldFill.INSERT)
    private Long addressBookId;


    //下单时间


    @ExcelProperty(value = "结账时间", converter = LocalDateStringConver.class)
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime orderTime;


    //结账时间
    @ExcelProperty(value = "结账时间", converter = LocalDateStringConver.class)
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkoutTime;


    //支付方式 1微信，2支付宝
    private Integer payMethod;


    //实收金额
    @ExcelProperty(value = "实收金额")
    private BigDecimal amount;

    //备注
    private String remark;

    //用户名
    @ExcelProperty(value = "用户名")
    private String userName;

    //手机号
    @ExcelProperty(value = "手机号")
    private String phone;

    //地址
    @ExcelProperty(value = "地址")
    private String address;

    //收货人

    private String consignee;
}
