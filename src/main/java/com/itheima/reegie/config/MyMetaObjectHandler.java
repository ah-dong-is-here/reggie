package com.itheima.reegie.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.itheima.reegie.common.BaseContext;
import com.itheima.reegie.entity.Employee;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * 自动填充处理类
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Long currentId = BaseContext.getCurrentId();
        log.info("获取的修改人值"+currentId);
        this.setFieldValByName("createUser", currentId, metaObject);
//        this.setFieldValByName("createUser", 1L, metaObject);
        this.setFieldValByName("createTime",LocalDateTime.now(), metaObject);
        this.setFieldValByName("updateTime",LocalDateTime.now(), metaObject);
        this.setFieldValByName("updateUser", currentId, metaObject);
//        this.setFieldValByName("updateUser",1L, metaObject);

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Long currentId = BaseContext.getCurrentId();
        this.setFieldValByName("updateUser", currentId, metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);

    }
}
