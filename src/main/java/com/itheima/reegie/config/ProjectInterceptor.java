
package com.itheima.reegie.config;


import com.itheima.reegie.common.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/*
//TODO：拦截器处理类
@Slf4j
@Component
public class ProjectInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
       //创建Session对象
        HttpSession session = request.getSession();
        //获取Session 查看请求中是否包含登录时传过来的Session
        Object attribute = session.getAttribute("id");
        Long id=(Long)attribute;
        BaseContext.setCurrentId(id);
        log.info("获取Session"+attribute);
        if(attribute!=null){
            return true;
        }else {
            response.sendRedirect("http://localhost:8080/backend/page/login/login.html");
            return false;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
*/


